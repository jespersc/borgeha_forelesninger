
''' Eksempel på kjøring:
>>> results = importResults(’Matches.txt’)
>>> print(results)
['Steinkjer,Byåsen,3-5', 'Byåsen,Steinkjer,2-1', 'Byåsen,Kvik Halden,2-10', 'Byåsen,Borg,0-0', 'Borg,Lade,3-2', 'Byåsen,Borg,0-0', 'Lade,Steinkjer,5-1', 'Nardo,Borg,3-3', 'Borg,Nardo,11-3', 'Steinkjer,Borg,2-2']
>>> 
'''
def importResults(filnavn): # Oppgave a)
    '''
    # - forsøke å hente inn fil
    # - hvis finnes, les, manipuler, returner
    - hvis ikke finnes, be brukeren om å skrive inn filnavn/q
    - håndtere unntak
    '''
    try:
        with open(filnavn, 'r', encoding='utf8') as f:
            alt = f.readlines()
            for i in range(len(alt)):
                alt[i] = alt[i].strip()
            return alt
        
    except:
        filnavn = input('Filnavn (q avslutter):')
        if filnavn == 'q':
            return None
        return importResults(filnavn)

# Linjene under kan du kjøre for å sjekke koden din:
# print(f"Denne skal returnere liste slik oppgaven definerer: {importResults('Matches.txt')}.")
# print(f"Denne skal utløse at man spør om nytt filnavn osv: {importResults('Matches.txt')}.")

# Import som vil måtte kjøres for at neste funksjon skal virke
# results = importResults('Matches.txt')
    
    
''' Eksempel på kjøring:
>>> results = importResults('Matches.txt')
>>> analyzed = analyzeResults(results)

>>> print(analyzed)
[['Steinkjer', 'Byåsen', 3, 5], ['Byåsen', 'Steinkjer', 2, 1], ...]
'''

def funksjonsnavn

# Linjene under kan du kjøre for å sjekke koden din (og brukes videre):
# analyzed = analyseResults(results)
# print(f"analyzed: {analyzed}.")


# analyzed = analyseResults(results)
# print(f"analyzed: {analyzed}.")


''' Eksempel på kjøring:
>>> match_result = calculateScores(3, 5)
>>> print(match_result)
(0, 3)
'''
def funksjonsnavn

# Linjene under kan du kjøre for å sjekke koden din:
# print(f"hjemmeseier: {calculateScores(3, 1)}.")
# print(f"borteseier: {calculateScores(3, 5)}.")
# print(f"uavgjort: {calculateScores(1, 1)}.")


''' Eksempel på bruk:
>>> analyzed = analyzeResults(results)
>>> team_data = sumTeamValues(analyzed)
>>> print(team_data)
{'Steinkjer': [1, 4], 'Byåsen': [8, 5], 'Kvik Halden': [3, 1], 'Borg': [10, 6], 'Lade': [3, 2], 'Nardo': [1, 2]}
'''
def funksjonsnavn

# Linjene under kan du kjøre for å sjekke koden din:
# team_data = sumTeamValues(analyzed)
# print(f"team_data: {team_data}.")

                
                
# Merk her: Den skal skrive ut resultatene fra oppgave b)
''' Eksempel på kjøring:
>>> showResults(analyzed)
#############################################
# Steinkjer      Byåsen          3 -  5 (B) #
# Byåsen         Steinkjer       2 -  1 (H) #
(og så videre)
'''
def funksjonsnavn

# Linjene under kan du kjøre for å sjekke koden din:
# showResults(analyseResults(results))      
        
''' Eksempel på kjøring:
>>> savePoints(team_data)
Team information saved to Points.txt.
'''     
def funksjonsnavn

# Linjene under kan du kjøre for å sjekke koden din:
# savePoints(team_data)