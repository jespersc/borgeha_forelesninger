import numpy as np
a = np.array([[1,2,3],[12,31,4],[27,8,9]])
if np.linalg.det(a) == 0:
    print('Kan ikke invertere matrisen')
else:
    b = np.linalg.inv(a)
    print(b)
    c = a@b
    d = b@a
    print(f'\n{c}\n')
    print(f'\n{d}\n')
    print(np.eye(c.shape[0]))
    